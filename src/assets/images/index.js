import Logo from './Logo.png';
import SplashBackground from './SplashBackground.png';
import ImageHeader from './header.png';
import DummyDoctor7 from './doctor7.png';
import DummyUser from './user.jpg'

export {Logo, SplashBackground, ImageHeader, DummyDoctor7, DummyUser}