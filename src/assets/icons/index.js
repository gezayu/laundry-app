import IconAkun from './akun.svg'
import IconAkunActive from './akunActive.svg'
import IconHome from './home.svg'
import IconHomeActive from './homeActive.svg'
import IconPesanan from './pesanan.svg'
import IconPesananActive from './pesananActive.svg'
import IconAddSaldo from './addSaldo.svg'
import IconGetPoint from './getPoint.svg'
import IconKiloan from './kiloan.svg'
import IconSatuan from './satuan.svg'
import IconVip from './vip.svg'
import IconKarpet from './karpet.svg'
import IconEkspress from './ekspress.svg'
import IconSetrika from './setrika_saja.svg'
import IconPesananAktif from './pesananAktif.svg'
import IconSendLight from './ic_send_light.svg';
import IconSendDark from './ic_send_dark.svg';
import IconBackDark from './ic_back_black.svg';
import IconBackWhite from './ic_back_white.svg';
import IconRemovePhoto from './btn_remove_photo.svg';
import ILLogo from './Logo.svg'

export {
    IconAkun,
    IconAkunActive, 
    IconHome, 
    IconHomeActive, 
    IconPesanan, 
    IconPesananActive, 
    IconAddSaldo, 
    IconGetPoint,
    IconKiloan,
    IconSatuan,
    IconVip,
    IconKarpet,
    IconSetrika,
    IconEkspress,
    IconPesananAktif,
    IconSendLight,
    IconSendDark,
    IconBackDark,
    IconBackWhite,
    IconRemovePhoto,
    ILLogo
}