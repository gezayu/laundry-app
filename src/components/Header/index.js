import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Gap, Button } from '../../components'
import {colors, fonts} from '../../utils'
import DarkProfile from './DarkProfile'

const Header = ({onPress, title, type}) => {
    if(type === 'dark-profile'){
        return <DarkProfile onPress={onPress}/>
    }
    return (
        <View style={styles.container(type)}>
            {/* <IconBackDark/> */}
            <Button type="icon-only" icon={type === 'dark' ? 'back-light' : 'back-light'} onPress={onPress} />
            <Text style={styles.textHeader(type)}>{title}</Text>
            <Gap width={24}/>
        </View>
    )
}

export default Header

const styles = StyleSheet.create({
    container : type =>({
            paddingHorizontal : 16,
            paddingVertical   :20,
            backgroundColor   : type === 'dark' ? '#55CB95' : '#55CB95',
            flexDirection     : 'row',
            alignItems        : 'center',
            borderBottomLeftRadius  : type === 'dark' ? 20 : 0,
            borderBottomRightRadius : type === 'dark' ? 20 : 0
        }),
    textHeader : type =>(
        {
            textAlign : 'center',
            flex : 1,
            fontSize : 20,
            fontFamily : fonts.primary[600],
            color : type === 'dark' ? colors.white : colors.text.primary
        }
    ) 
})
