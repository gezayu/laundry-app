import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { DummyDoctor7 } from '../../assets'
import { colors, fonts } from '../../utils'
import { Button } from '../Button'

const DarkProfile = ({onPress}) => {
    return (
        <View style={styles.container}>
            <Button type="icon-only" icon="back-light" onPress={onPress}/>
            <View style={styles.content}>
                <Text style={styles.name}>Hitomi Minatozaki Sana</Text>
                <Text style={styles.category}>Dokter Anak</Text>
            </View>
            <Image source={DummyDoctor7} style={styles.avatar}/>
        </View>
    )
}

export default DarkProfile

const styles = StyleSheet.create({
    container : {
        backgroundColor : colors.secondary,
        paddingVertical : 30,
        paddingLeft : 20,
        paddingRight : 16,
        borderBottomLeftRadius : 20,
        borderBottomRightRadius : 20,
        flexDirection : 'row',
        alignItems : 'center'
    },
    content : {
        flex : 1
    },
    avatar : {
        width : 46,
        height : 46,
        borderRadius : 46 / 2
    },
    name : {
        fontSize : 20,
        fontFamily : fonts.primary[600],
        color : colors.white,
        textAlign : 'center'
    },
    category : {
        fontSize : 14,
        color : colors.text.subTitle,
        fontFamily : fonts.primary.normal,
        textAlign : 'center',
        marginTop : 6
    }
})
