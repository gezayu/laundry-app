import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {Splash, Home, Pesanan, Akun, Login, Register, UserDetailScreen} from '../pages';
import {BottomNavigator} from '../components'
import UserScreen from '../pages/UserScreen';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return (
  <Tab.Navigator tabBar={props => <BottomNavigator {...props} />}>
    <Tab.Screen name="Home" component={Home} />
    <Tab.Screen name="Pesanan" component={Pesanan}/>
    <Tab.Screen name="Akun" component={Akun}/>
  </Tab.Navigator>
  )
}

const Router = () => {
    return (
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen name="Splash" component={Splash} options={{headerShown:false}} />
        <Stack.Screen name="MainApp" component={MainApp} options={{headerShown:false}} />
        <Stack.Screen name="Login" component={Login} options={{headerShown:false}} />
        <Stack.Screen name="Register" component={Register} options={{headerShown:false}} />
        <Stack.Screen name="UserScreen" component={UserScreen} options={{headerShown:false}} />
        <Stack.Screen name="UserDetailScreen" component={UserDetailScreen} options={{headerShown:false}} />
      </Stack.Navigator>
    )
}

export default Router

const styles = StyleSheet.create({})
