import React, { useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { Header, Input, Button, Gap } from '../../components'
import { colors } from '../../utils'

const Register = ({navigation}) => {
    return (
        <View style={styles.page}>
            <Header onPress = {() => navigation.goBack()} title="Daftar Akun"/>
            <View style={styles.content}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Gap height={20}/>
                    <Input label="Nama Lengkap"/>
                    <Gap height={24}/>
                    <Input label="Pekerjaan"/>
                    <Gap height={24}/>
                    <Input label="Email"/>
                    <Gap height={24}/>
                    <Input label="Password"/>
                    <Gap height={40}/>
                    <Button title ="Simpan" onPress={() => navigation.navigate('Login')}/>
                </ScrollView>
            </View>
        </View>
    )
}

export default Register

const styles = StyleSheet.create({
    page : {
        backgroundColor : colors.white,
        flex : 1
    },
    content : {
        padding : 37,
        paddingTop : 0
    }
})
