import React from 'react'
import { Dimensions, ImageBackground, StyleSheet, Text, View, Image } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { ImageHeader, Logo } from '../../assets'
import { ButtonIcon, Saldo, PesananAktif } from '../../components/'
import { WARNA_ABU_ABU } from '../../utils/constant'

const Home = () => {
    return (
        <View style={styles.page}>

            <ScrollView showsVerticalScrollIndicator={false}>

                {/* ini Komponen Headernya */}
                <ImageBackground
                    source={ImageHeader}
                    style={styles.header}>
                    <Image source={Logo} style={styles.logo} />
                    <View style={styles.hello}>
                        <Text style={styles.selamat}>Selamat datang,</Text>
                        <Text style={styles.username}>Geza Fiqrullah</Text>
                    </View>
                </ImageBackground>

                {/* ini cardview saldo */}
                <Saldo />

                {/* ini komponen icon-icon layanan */}
                <View style={styles.layanan}>
                    <Text style={styles.label}>Layanan kami</Text>
                    <View style={styles.iconLayanan}>
                        <ButtonIcon title="Kiloan" type="layanan" />
                        <ButtonIcon title="Satuan" type="layanan" />
                        <ButtonIcon title="VIP" type="layanan" />
                        <ButtonIcon title="Karpet" type="layanan" />
                        <ButtonIcon title="Setrika Saja" type="layanan" />
                        <ButtonIcon title="Ekspress" type="layanan" />
                    </View>
                </View>

                {/* ini komponen pesanan */}
                <View style={styles.pesananAktif}>
                    <Text style={styles.label}>Pesanan Aktif</Text>
                    <PesananAktif title="Pesanan No. 000128" status="Sudah Selesai" />
                    <PesananAktif title="Pesanan No. 000127" status="Masih Dicuci" />
                    <PesananAktif title="Pesanan No. 000126" status="Sudah Selesai" />
                    <PesananAktif title="Pesanan No. 000125" status="Masih Dicuci" />
                    <PesananAktif title="Pesanan No. 000124" status="Sudah Selesai" />
                </View>
            </ScrollView>
        </View>
    )
}

export default Home

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: 'white'
    },
    header: {
        width: windowWidth,
        height: windowHeight * 0.30,
        paddingHorizontal: 30,
        paddingTop: 10
    },
    logo: {
        width: windowWidth * 0.25,
        height: windowHeight * 0.06
    },
    hello: {
        marginTop: windowHeight * 0.030
    },
    selamat: {
        fontSize: 22,
        fontFamily: 'TitilliumWeb-Regular'
    },
    username: {
        fontSize: 19,
        fontFamily: 'TitilliumWeb-Bold'
    },
    layanan: {
        paddingLeft: 30,
        paddingTop: 22
    },
    label: {
        fontSize: 18,
        fontFamily: 'TitilliumWeb-Bold'
    },
    iconLayanan: {
        flexDirection: 'row',
        marginTop: 8,
        justifyContent: 'space-between',
        flexWrap: 'wrap'
    },
    pesananAktif: {
        padding: 10,
        paddingHorizontal: 30,
        backgroundColor: WARNA_ABU_ABU,
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20
    }
})
