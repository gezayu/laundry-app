import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Button, Gap, Header, Profile, ProfileItem } from '../../components'
import { colors } from '../../utils'

const Akun = ({navigation}) => {
    return (
        <View style={styles.page}>
            <Header title="Akun" onPress={() => navigation.goBack()} type={'dark'}/>
            <Gap height={25}/>
            <Profile name="Geza Fiqrullah Alamsyah V.B.W" desc="Founder Of Laundry App"/>
            <Gap height={16}/>
            <ProfileItem label="Tempat dan Tanggal Lahir" value="Malang,14 Desember 2021"/>
            <ProfileItem label="Riwayat Pendidikan" value="SMKN 8 Malang"/>
            <ProfileItem label="Email" value="gezayu@gmail.com"/>
            <View style={styles.action}>
            <Button title="Log Out" onPress={() => navigation.navigate('Login')}/>
            </View>
        </View>
    )
}

export default Akun

const styles = StyleSheet.create({
    page : {
        backgroundColor : 'white',
        flex : 1
    },
    action : {
        paddingHorizontal : 40,
        paddingTop : 23
    }
})
