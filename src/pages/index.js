import Home from './Home';
import Akun from './Akun';
import Pesanan from './Pesanan';
import Splash from './Splash';
import Login from './Login';
import Register from './Register'
import UserScreen from './UserScreen'
import UserDetailScreen from './UserDetailScreen'


export{Splash, Home, Pesanan, Akun, Login, Register, UserScreen, UserDetailScreen}