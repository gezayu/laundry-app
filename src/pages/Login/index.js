import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { ILLogo } from '../../assets';
import { Input, Link, Button, Gap } from '../../components';
import { colors, fonts } from '../../utils';

const Login = ({navigation}) => {
    return (
        <View style={styles.page}>
            <Image source={require('../../assets/images/Logo.png')} style={{width : 160,height:70,right:30}}/>
            <Text style={styles.title}>Masuk dan mulai pesan sekarang</Text>
            <Input label ="Email"/>
            <Gap height={24}/>
            <Input label ="Password"/>
            <Gap height={10}/>
            <Link title="Lupa Password" size={12}/>
            <Gap height={40}/>
            <Button title="Login" onPress={() => navigation.replace('MainApp')}/>
            <Gap height={30}/>
            <Link title="Create New Account" size={15} align="center" onPress={() => navigation.navigate('Register')}/>
        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    page : {
        padding : 40,
        flex : 1,
        backgroundColor : colors.white
    },
    title : {
        fontSize : 20,
        fontFamily : fonts.primary[600],
        marginVertical : 40,
        color : colors.text.primary,
        maxWidth : 153
    }
})
