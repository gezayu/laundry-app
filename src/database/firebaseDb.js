import * as firebase from 'firebase/app';
import 'firebase/firestore';

const firebaseConfig = {
    databaseURL: "https://reactnativefirebase-55819.firebaseio.com",
    apiKey: "AIzaSyDw1dmRmYgvB4jo6jHSlsA4pEBBs6OjKEo",
    authDomain: "reactnativefirebase-55819.firebaseapp.com",
    projectId: "reactnativefirebase-55819",
    storageBucket: "reactnativefirebase-55819.appspot.com",
    messagingSenderId: "819097862996",
    appId: "1:819097862996:web:7670447ca05d37fa7ffec1",
    
};

firebase.initializeApp(firebaseConfig);

firebase.firestore();

export default firebase;